package topapp.btmodule;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.inaka.galgo.Galgo;
import com.inaka.galgo.GalgoOptions;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.UUID;

import static android.bluetooth.BluetoothAdapter.STATE_CONNECTED;

public class MainActivity extends AppCompatActivity {


    public static final String DEVICE = "MYGUNDEVICE";

    private static final int REQUEST_ENABLE_BT = 999;
    private static final int REQUEST_BT_DEVICE = 111;
    private BluetoothAdapter mBluetoothAdapter;

    // UUIDs
    static final UUID NORDIC_UART_SERVICE = UUID.fromString("6E400001-B5A3-F393-E0A9-E50E24DCCA9E");
    static final UUID RX_UUID = UUID.fromString("6E400002-B5A3-F393-E0A9-E50E24DCCA9E");
    static final UUID TX_UUID = UUID.fromString("6E400003-B5A3-F393-E0A9-E50E24DCCA9E");
    static final UUID CLIENT_CHARACTERISTIC_CONFIG_UUID = convertFromInteger(0x2902);

    boolean isWriting;
    boolean isLearning;


    TextView gyrx;
    TextView gyry;
    TextView gyrz;

    TextView accx;
    TextView accy;
    TextView accz;

    TextView matrix;

    Button button;
    Button learnButton;

    double rotx = 0;
    double roty = 0;
    double rotz = 0;
    final double SECONDSPERPACKAGE = 0.1;

    SensorUtils mSensorUtils;

    LinearLayout mGraphlive;
    LinearLayout mTextlive;



    topapp.btmodule.Chronometer chronometer;

    BluetoothGatt gatt;

    GraphView graphView1;
    GraphView graphView2;

    BluetoothAdapter.LeScanCallback scanCallback;

    BluetoothDevice myDevice_myGun;

    BluetoothGattCallback gattCallback;

    TextView text_myDevice;

    boolean GalgoEnabled = false;

    DecimalFormat df = new DecimalFormat("#.##");

    StringWriter logWriter;
    StringWriter logWriter2;

    int x1;

    private LineGraphSeries<DataPoint> mSeriesGyrX;
    private LineGraphSeries<DataPoint> mSeriesGyrY;
    private LineGraphSeries<DataPoint> mSeriesGyrZ;
    private LineGraphSeries<DataPoint> mSeriesAccX;
    private LineGraphSeries<DataPoint> mSeriesAccY;
    private LineGraphSeries<DataPoint> mSeriesAccZ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        isWriting=false;

        logWriter = new StringWriter();
        logWriter2 = new StringWriter();

        mGraphlive = (LinearLayout) findViewById(R.id.graphlive);
        mTextlive = (LinearLayout) findViewById(R.id.textlive);


        text_myDevice = (TextView) findViewById(R.id.t1);

        gyrx = (TextView) findViewById(R.id.gyrx);
        gyry = (TextView) findViewById(R.id.gyry);
        gyrz = (TextView) findViewById(R.id.gyrz);

        accx = (TextView) findViewById(R.id.accx);
        accy = (TextView) findViewById(R.id.accy);
        accz = (TextView) findViewById(R.id.accz);

        button = (Button) findViewById(R.id.button2);
        learnButton = (Button) findViewById(R.id.button6);

        chronometer =  findViewById(R.id.time);

        graphView1 = (GraphView) findViewById(R.id.graph1);
        graphView2 = (GraphView) findViewById(R.id.graph2);

        mSensorUtils = new SensorUtils();

        // Check if BLE is possible on this phone
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }
        // Initializes Bluetooth adapter.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Ensures Bluetooth is available on the device and it is enabled. If not,
        // displays a dialog requesting user permission to enable Bluetooth.
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        mSeriesGyrX = new LineGraphSeries<>();
        mSeriesGyrX.setColor(Color.rgb(255,0,0));
        mSeriesGyrY = new LineGraphSeries<>();
        mSeriesGyrY.setColor(Color.rgb(0,255,0));
        mSeriesGyrZ = new LineGraphSeries<>();
        mSeriesGyrZ.setColor(Color.rgb(0,0,255));
        mSeriesAccX = new LineGraphSeries<>();
        mSeriesAccX.setColor(Color.rgb(255,0,0));
        mSeriesAccY = new LineGraphSeries<>();
        mSeriesAccY.setColor(Color.rgb(0,255,0));
        mSeriesAccZ = new LineGraphSeries<>();
        mSeriesAccZ.setColor(Color.rgb(0,0,255));


        graphView1.addSeries(mSeriesGyrX);
        graphView1.addSeries(mSeriesGyrY);
        graphView1.addSeries(mSeriesGyrZ);
        graphView2.addSeries(mSeriesAccX);
        graphView2.addSeries(mSeriesAccY);
        graphView2.addSeries(mSeriesAccZ);

        x1=0;



    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_BT_DEVICE) {
            if(resultCode == Activity.RESULT_OK){
                ArrayList<BluetoothDevice> blue =  data.getExtras().getParcelableArrayList(DEVICE);
                myDevice_myGun = blue.get(0);
                text_myDevice.setText("Device connected: "+myDevice_myGun.getName()+" "+myDevice_myGun.getAddress());
                startReading();

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(),"No Device returned", Toast.LENGTH_SHORT).show();
            }
        }
    }//onActivityResult

    public void findmyGun(View view){
        Intent mDeviceScanActivity= new Intent(this,DeviceScanActivity.class);
        startActivityForResult(mDeviceScanActivity, REQUEST_BT_DEVICE);
    }

    public void startReading(){
        gattCallback = new BluetoothGattCallback() {
            @Override
            public void onPhyUpdate(BluetoothGatt gatt, int txPhy, int rxPhy, int status) {
                super.onPhyUpdate(gatt, txPhy, rxPhy, status);
            }

            @Override
            public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                super.onConnectionStateChange(gatt, status, newState);
                if (newState == STATE_CONNECTED){
                    gatt.discoverServices();
                }
            }

            @Override
            public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                super.onServicesDiscovered(gatt, status);

                /*BluetoothGattCharacteristic rxCharacteristic = gatt.getService(NORDIC_UART_SERVICE).getCharacteristic(RX_UUID);
                rxCharacteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
                rxCharacteristic.setValue(new byte[]{1});*/
                BluetoothGattCharacteristic characteristic = gatt.getService(NORDIC_UART_SERVICE).getCharacteristic(TX_UUID); // MAKE SURE YOU ARE GETTING RIGHT CHARACTERISTIC
                gatt.setCharacteristicNotification(characteristic, true);

                BluetoothGattDescriptor descriptor =
                        characteristic.getDescriptor(CLIENT_CHARACTERISTIC_CONFIG_UUID);

                descriptor.setValue(
                        BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                gatt.writeDescriptor(descriptor);



            }

            @Override
            public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
                super.onDescriptorWrite(gatt, descriptor, status);

                BluetoothGattCharacteristic characteristic =
                        gatt.getService(NORDIC_UART_SERVICE).getCharacteristic(RX_UUID);
                characteristic.setValue(new byte[]{1});
                gatt.writeCharacteristic(characteristic);
                /*BluetoothGattCharacteristic rxCharacteristic = new BluetoothGattCharacteristic(RX_UUID,
                        BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE,
                        BluetoothGattCharacteristic.PERMISSION_WRITE);
                rxCharacteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
                rxCharacteristic.setValue(new byte[]{1,1});*/


            }

            @Override
            public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
                super.onCharacteristicChanged(gatt, characteristic);
                processData(characteristic.getValue());
            }
        };

        gatt = myDevice_myGun.connectGatt(this,true,gattCallback);

        rotx = 0;
        roty = 0;
        rotz = 0;

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mBluetoothAdapter.stopLeScan(scanCallback);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mBluetoothAdapter.stopLeScan(scanCallback);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mBluetoothAdapter.stopLeScan(scanCallback);
    }

    public static UUID convertFromInteger(int i) {
        final long MSB = 0x0000000000001000L;
        final long LSB = 0x800000805f9b34fbL;
        long value = i & 0xFFFFFFFF;
        return new UUID(MSB | (value << 32), LSB);
    }

    public void processData(byte[] data){

        String stringData = "";

        //Log.d("TIMECHECK",(System.currentTimeMillis())+" ");

        short raw_GyroX = (short) (( (data[1]  & 0xff )<< 8) | (data[0] & 0xff) );
        short raw_GyroY = (short) (( (data[3]  & 0xff )<< 8) | (data[2] & 0xff) );
        short raw_GyroZ = (short) (( (data[5]  & 0xff )<< 8) | (data[4] & 0xff) );
        short raw_AcceX = (short) (( (data[7]  & 0xff )<< 8) | (data[6] & 0xff) );
        short raw_AcceY = (short) (( (data[9]  & 0xff )<< 8) | (data[8] & 0xff) );
        short raw_AcceZ = (short) (( (data[11]  & 0xff )<< 8) | (data[10] & 0xff) );


        double val_GyroX = ((raw_GyroX*8750+500)/1000)/1000; // od 0 do 360
        double val_GyroY = ((raw_GyroY*8750+500)/1000)/1000; // deg/sec
        double val_GyroZ = ((raw_GyroZ*8750+500)/1000)/1000;

        double val_AcceX = ((raw_AcceX * 61 + 500)/1000); // od -2g do 2g
        double val_AcceY = ((raw_AcceY * 61 + 500)/1000); // mil g ( gravitacijski pospešek )
        double val_AcceZ = ((raw_AcceZ * 61 + 500)/1000);

        mSensorUtils.getData(val_GyroX,val_GyroY,val_GyroZ,val_AcceX,val_AcceY,val_AcceZ);

        final double txRotx = mSensorUtils.getRotX();
        final double txRoty = mSensorUtils.getRotY();
        final double txRotz = mSensorUtils.getRotZ();
        final double txAccx = mSensorUtils.getAccX();
        final double txAccy = mSensorUtils.getAccY();
        final double txAccz = mSensorUtils.getAccZ();

        String processedData = val_GyroX+" "+val_GyroY+" "+val_GyroZ+" "+val_AcceX+" "+val_AcceY+" "+val_AcceZ;
        String rawData = raw_GyroX+" "+raw_GyroY+" "+raw_GyroZ+" "+raw_AcceX+" "+raw_AcceY+" "+raw_AcceZ;
        String stringDataRaw = chronometer.getText().toString()+" "+rawData+" "+processedData+" ";
        logWriter.write(stringDataRaw);
        if(isLearning){logWriter2.write(stringDataRaw);}
        if(isWriting){Log.d("LOG",stringDataRaw);}



        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                    gyrx.setText("Rot X: "+df.format(txRotx));

                    gyry.setText("Rot Y: "+df.format(txRoty));

                    gyrz.setText("Rot Z: "+df.format(txRotz));

                    accx.setText("Acc X: "+df.format(txAccx));

                    accy.setText("Acc Y: "+df.format(txAccy));

                    accz.setText("Acc Z: "+df.format(txAccz));

                    mSeriesGyrX.appendData(new DataPoint(x1,txRotx),false,250);
                    mSeriesGyrY.appendData(new DataPoint(x1,txRoty),false,250);
                    mSeriesGyrZ.appendData(new DataPoint(x1,txRotz),false,250);
                    mSeriesAccX.appendData(new DataPoint(x1,txAccx),false,250);
                    mSeriesAccY.appendData(new DataPoint(x1,txAccy),false,250);
                    mSeriesAccZ.appendData(new DataPoint(x1,txAccz),false,250);
                    x1++;

            }

        });

    }


    public void showhideLog(View view){

        if(!GalgoEnabled){
            GalgoOptions options = new GalgoOptions.Builder()
                    .numberOfLines(15)
                    .backgroundColor(Color.parseColor("#D9d6d6d6"))
                    .textColor(Color.BLACK)
                    .textSize(12)
                    .build();
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.SYSTEM_ALERT_WINDOW)
                    != PackageManager.PERMISSION_GRANTED) {
                Galgo.enable(this, options);
                GalgoEnabled=true;
            } else {
                Toast.makeText(this,"NO SYSTEM ALERT PERMISSION", Toast.LENGTH_SHORT);
            }

        } else {
            Galgo.disable(this);
            GalgoEnabled=false;
        }

    }
    public void reset(View view){
        mSensorUtils.resetRot();
    }
    public void saveLog(View view){

            if(!isWriting){
                StringBuffer SB = logWriter.getBuffer();
                SB.delete(0,SB.length());
                chronometer.setBase(SystemClock.elapsedRealtime());
                chronometer.start();
                isWriting=true;
                button.setText("STOP AND SAVE");


            } else {
                chronometer.stop();
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
                alertDialog.setMessage("Save As:");
                final EditText input = new EditText(MainActivity.this);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                input.setLayoutParams(lp);
                alertDialog.setView(input); // uncomment this line

                alertDialog.setPositiveButton("Done",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int which) {
                                File myLog = new File("/sdcard/"+input.getText()+"-MyGunRAWDATA.txt");
                                FileWriter fw = null;
                                try {
                                    myLog.createNewFile();
                                    fw = new FileWriter(myLog);
                                    fw.write(logWriter.toString());
                                    fw.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                Toast.makeText(getApplicationContext(),"Log Saved", Toast.LENGTH_SHORT).show();

                            }
                        });
                alertDialog.show();
                isWriting=false;
                button.setText("START");
            }



    }
    public void openGraphsActivity(View view)
    {
        Intent intent = new Intent(this,OpenGraphActivity.class);
        startActivity(intent);
    }
    public void toggleGraph(View view)
    {
        if(mGraphlive.getVisibility()==View.GONE){
            mTextlive.setVisibility(View.GONE);
            mGraphlive.setVisibility(View.VISIBLE);
        } else {
            mTextlive.setVisibility(View.VISIBLE);
            mGraphlive.setVisibility(View.GONE);
        }
    }
    public void teachMove(View view){
        if(!isLearning){
            learnButton.setText("STOP TEACHING");
        } else {
            learnButton.setText("START TEACHING");
            String[] data = logWriter2.toString().split(" ");

        }
        isLearning=!isLearning;
    }
}

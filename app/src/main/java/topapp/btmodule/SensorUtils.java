package topapp.btmodule;

import android.util.Log;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import java.text.DecimalFormat;

public class SensorUtils {

    double[] rot = new double[3];



    RealMatrix matrix_rot ; //Matrix represents a rotation of device between the last and the new reading
    RealMatrix finalMatrix;// = MatrixUtils.createRealMatrix(new double[][]{{1,0,0},{0,1,0},{0,1,0}});

    double[] gravity;

    double[] acc = new double[3];

    DecimalFormat df = new DecimalFormat("#.##");

    double[] velocity = {0,0,0};
    double[] position = {0,0,0};

    final double SECONDSPERPACKAGE = 0.1;

    public double getRotX() { return this.rot[0]; }
    public double getRotY(){
        return this.rot[1];
    }
    public double getRotZ(){
        return this.rot[2];
    }


    public void getData(double gyrx, double gyry,double gyrz, double accx, double accy, double accz){
        if(isStill(gyrx,gyry,gyrz,accx,accy,accz)){
            return;
        }
        addRota(gyrx,gyry,gyrz);
        double accx2 = Math.sqrt(accx/1000)*9.80665;
        double accy2 = Math.sqrt(accy/1000)*9.80665;
        double accz2 = Math.sqrt(accz/1000)*9.80665;
        addAcce(accx2,accy2,accz2);

    }


    public void addRota(double x, double y, double z){

        double[] zacasni;

        // Adds curr_rot change to rotation
        rot[0]=addRot(rot[0],x);
        rot[1]=addRot(rot[1],y);
        rot[2]=addRot(rot[2],z);
        // Converts degrees to matrix

        double magnitude = Math.sqrt(Math.pow(rot[0],2)+Math.pow(rot[1],2)+Math.pow(rot[2],2));

        if(magnitude==0){
            zacasni =new double[]{rot[0],rot[1],rot[2]};
        }else {

            zacasni =new double[]{rot[0]/magnitude,rot[1]/magnitude,rot[2]/magnitude};
        }

        matrix_rot= convertToMatrix(zacasni,magnitude);
        accumulateOrientation(matrix_rot);
        //Log.d("ROTATION",finalMatrix.getEntry(0,0)+" "+finalMatrix.getEntry(0,1)+" "+finalMatrix.getEntry(0,2)+" "
        //            +finalMatrix.getEntry(1,0)+" "+finalMatrix.getEntry(1,1)+" "+finalMatrix.getEntry(1,2)+" "+
        //            finalMatrix.getEntry(2,0)+" "+finalMatrix.getEntry(2,1)+" "+finalMatrix.getEntry(2,2));
    }
    public void accumulateOrientation(RealMatrix matrix){
        if(finalMatrix==null)
        {
            finalMatrix = matrix;
        } else {
            finalMatrix = finalMatrix.multiply(matrix);
        }

    }
    public double addRot(double y, double x){
        y+=x* SECONDSPERPACKAGE;
        return y%360;
    }
    public double deltaIntegrate(double y, double x){
        y+=x* SECONDSPERPACKAGE;
        return y;
    }
    public RealMatrix convertToMatrix(double[] rotation, double magnitude){

        RealVector smer = MatrixUtils.createRealVector(rotation);

        double magnitude2 = Math.sqrt(Math.pow(rotation[0],2)+Math.pow(rotation[1],2)+Math.pow(rotation[2],2));


        double a = rotation[0]; double b = rotation[1]; double c = rotation[2];

        float a1 = (float) (Math.pow(a,2)+ (1 - Math.pow(a,2))*Math.cos(magnitude));
        float a2 = (float) (a*b*(1-Math.cos(magnitude))-c*Math.sin(magnitude));
        float a3 = (float) (a*c*(1-Math.cos(magnitude))+b*Math.sin(magnitude));
        float b1 = (float) (a*b*(1-Math.cos(magnitude))+c*Math.sin(magnitude));
        float b2 = (float) (Math.pow(b,2)+ (1 - Math.pow(b,2))*Math.cos(magnitude));
        float b3 = (float) (b*c*(1-Math.cos(magnitude))-a*Math.sin(magnitude));
        float c1 = (float) (a*c*(1-Math.cos(magnitude))-b*Math.sin(magnitude));
        float c2 = (float) (b*c*(1-Math.cos(magnitude))+a*Math.sin(magnitude));
        float c3 = (float) (Math.pow(c,2)+ (1 - Math.pow(c,2))*Math.cos(magnitude));

        RealMatrix matrix = MatrixUtils.createRealMatrix(new double[][]{{a1,a2,a3},{b1,b2,b3},{c1,c2,c3}});
        return matrix;

        /*for(int i = 0; i<rotation.length;i++){
            matrix[i]= Math.cos(rotation[i]);
        }*/

    }


    public void resetRot(){

        rot[0] = 0;
        rot[1] = 0;
        rot[2] = 0;

        position = new double[]{0,0,0};

        gravity=null;

    }


    public void addAcce(double x2,double y2,double z2)
    {

        double[] acce = new double[]{x2,y2,z2};
        setGravity(acce);

        RealVector vector = MatrixUtils.createRealVector(acce);
        RealVector temp_acc_vector = matrix_rot.preMultiply(vector);



        acc = new double[]{deltaIntegrate(acc[0],temp_acc_vector.getEntry(0)-gravity[0]),deltaIntegrate(acc[1],temp_acc_vector.getEntry(1)-gravity[1]),deltaIntegrate(acc[2],temp_acc_vector.getEntry(2)-gravity[2])};

        double[] vel = new double[]{velocity[0]+acc[0],velocity[1]+acc[1],velocity[2]+acc[2]};

        velocity = new double[]{deltaIntegrate(velocity[0],vel[0]),deltaIntegrate(velocity[1],vel[1]),deltaIntegrate(velocity[2],vel[2])};

        Log.d("VELOCITY",df.format(velocity[0]/100)+"m/s "+df.format(velocity[1]/100)+"m/s "+df.format(velocity[2]/100)+"m/s");
        position = new double[]{position[0]+velocity[0],position[1]+velocity[1],position[2]+velocity[2]};

    }
    public boolean isStill(double x, double y, double z, double x2, double y2, double z2){
        double var = Math.sqrt((Math.pow(x,2)+Math.pow(y,2)+Math.pow(z,2)));
        if(var<5)
        {
            double var2 = Math.sqrt((Math.pow(x2,2)+Math.pow(y2,2)+Math.pow(z2,2)));
            if(960<var2&&var2<1040)
            {

                velocity = new double[] {0,0,0};
                gravity = null;
                return true;
            }
        }
        return false;

    }

    public void setGravity(double[] grav){
        if(gravity==null){
            gravity=grav;
        }
    }


    public double getAccX(){
        return this.position[0];
    }
    public double getAccY(){
        return this.position[1];
    }
    public double getAccZ(){
        return this.position[2];
    }
}

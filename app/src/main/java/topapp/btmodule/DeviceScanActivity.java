package topapp.btmodule;

import android.app.Activity;
import android.app.ListActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Activity for scanning and displaying available BLE devices.
 */
public class DeviceScanActivity extends ListActivity {

    private BluetoothAdapter mBluetoothAdapter;
    private Handler mHandler;
    BluetoothAdapter.LeScanCallback mLeScanCallback;
    private boolean mScanning;
    private TextView noresult_TextView;



    // Stops scanning after 10 seconds.
    private static final long SCAN_PERIOD = 10000;

    //LIST OF ARRAY STRINGS WHICH WILL SERVE AS LIST ITEMS
    ArrayList<String> listItems=new ArrayList<String>();
    ArrayAdapter<String> adapter;
    ArrayList<BluetoothDevice> btItems = new ArrayList<BluetoothDevice>();



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scan_ble_list);

        noresult_TextView = (TextView) findViewById(R.id.noresult);


        adapter=new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                listItems);
        setListAdapter(adapter);
        ListView lv = getListView();
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long arg3)
            {
                BluetoothDevice dev = btItems.get(position);
                if(dev.getBondState()==BluetoothDevice.BOND_NONE){
                    dev.createBond();
                }
                Intent data = new Intent();
                ArrayList<BluetoothDevice> devicc = new ArrayList<>();
                devicc.add(dev);
                data.putParcelableArrayListExtra(MainActivity.DEVICE,devicc);
                setResult(Activity.RESULT_OK,data);
                finish();
            }
        });


        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        mHandler = new Handler();

        mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
            @Override
            public void onLeScan(BluetoothDevice bluetoothDevice, int i, byte[] bytes) {
                addItems(bluetoothDevice);
            }
        };
        alreadyconnected();
        //scanLeDevice(true);
    }
    //METHOD WHICH WILL HANDLE DYNAMIC INSERTION
    public void addItems(BluetoothDevice bt) {
        if(!listItems.contains(bt.getName()+ " Add: "+bt.getAddress())){
            listItems.add(bt.getName()+ " Add: "+bt.getAddress() );
            btItems.add(bt);
        }
        adapter.notifyDataSetChanged();
    }


    private void alreadyconnected(){
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

        List<String> s = new ArrayList<String>();
        for(BluetoothDevice bt : pairedDevices)
        {
            addItems(bt);
        }
    }

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    stopscanLeDevice();
                }
            }, SCAN_PERIOD);

            mScanning = true;
            mBluetoothAdapter.startLeScan(mLeScanCallback);
        } else {
            stopscanLeDevice();
        }

    }

    private void stopscanLeDevice(){
        if(listItems.size()==0){
            noresult_TextView.setVisibility(View.VISIBLE);
        }
        mScanning = false;
        mBluetoothAdapter.stopLeScan(mLeScanCallback);
    }


}

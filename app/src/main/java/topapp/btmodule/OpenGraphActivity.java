package topapp.btmodule;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class OpenGraphActivity extends AppCompatActivity {

    private static final int ACTIVITY_CHOOSE_FILE = 7878;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 22232;

    TextView logTextView;
    String[] data;
    List<String> timedata;
    List<String> rawdata;
    List<String> calcdata;
    GraphView graphView1;
    GraphView graphView2;

    DecimalFormat df = new DecimalFormat("#.##");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_graph);
        logTextView = (TextView) findViewById(R.id.logTV);
        graphView1 = (GraphView) findViewById(R.id.graph1);
        graphView2 = (GraphView) findViewById(R.id.graph2);
        timedata = new ArrayList<String>();
        rawdata = new ArrayList<String>();
        calcdata = new ArrayList<String>();



    }
    public void onBrowse(View view) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
        } else {
            Intent chooseFile;
            Intent intent;
            chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
            chooseFile.addCategory(Intent.CATEGORY_OPENABLE);
            chooseFile.setType("text/plain");
            intent = Intent.createChooser(chooseFile, "Choose a Log");
            startActivityForResult(intent, ACTIVITY_CHOOSE_FILE);
        }

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) return;
        String path     = "";
        if(requestCode == ACTIVITY_CHOOSE_FILE)
        {
            Uri uri = data.getData();
            String FilePath = uri.getLastPathSegment(); // should the path be here in this string
            logTextView.setText(FilePath);
            readTextFile(uri);
        }
    }
    private void readTextFile(Uri uri){
        BufferedReader reader = null;
        StringBuilder builder = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(getContentResolver().openInputStream(uri)));
            String line = "";

            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null){
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        data = builder.toString().split(" ");
        processData();
    }
    private void processData(){
        for(int i = 0; i+13 < data.length;i+=13)
        {
            timedata.add(data[i]);
            rawdata.add(data[i+1]);
            rawdata.add(data[i+2]);
            rawdata.add(data[i+3]);
            rawdata.add(data[i+4]);
            rawdata.add(data[i+5]);
            rawdata.add(data[i+6]);
            calcdata.add(data[i+7]);
            calcdata.add(data[i+8]);
            calcdata.add(data[i+9]);
            calcdata.add(data[i+10]);
            calcdata.add(data[i+11]);
            calcdata.add(data[i+12]);
        }
        updateGraph();
    }
    private void updateGraph(){
        graphView1.getViewport().setScalable(true);
        graphView2.getViewport().setScalable(true);

        DataPoint[] gyrxrawdataPoints = new DataPoint[rawdata.size()/6];
        DataPoint[] gyryrawdataPoints = new DataPoint[rawdata.size()/6];
        DataPoint[] gyrzrawdataPoints = new DataPoint[rawdata.size()/6];
        DataPoint[] accxrawdataPoints = new DataPoint[rawdata.size()/6];
        DataPoint[] accyrawdataPoints = new DataPoint[rawdata.size()/6];
        DataPoint[] acczrawdataPoints = new DataPoint[rawdata.size()/6];



        for(int i = 0; i + 6 < rawdata.size();i+=6){
            int s = i/6;
            gyrxrawdataPoints[s]=(new DataPoint(s+1, Double.parseDouble(rawdata.get(i))));
            gyryrawdataPoints[s]=(new DataPoint(s+1, Double.parseDouble(rawdata.get(i+1))));
            gyrzrawdataPoints[s]=(new DataPoint(s+1, Double.parseDouble(rawdata.get(i+2))));
            accxrawdataPoints[s]=(new DataPoint(s+1, Double.parseDouble(rawdata.get(i+3))));
            accyrawdataPoints[s]=(new DataPoint(s+1, Double.parseDouble(rawdata.get(i+4))));
            acczrawdataPoints[s]=(new DataPoint(s+1, Double.parseDouble(rawdata.get(i+5))));




        }
        for(int i = 0; i<rawdata.size()/6;i++){
            if(gyrxrawdataPoints[i]==null){
                gyrxrawdataPoints[i] =  new DataPoint(i+1, 0);
            }
            if(gyryrawdataPoints[i]==null){
                gyryrawdataPoints[i] =  new DataPoint(i+1, 0);
            }
            if(gyrzrawdataPoints[i]==null){
                gyrzrawdataPoints[i] =  new DataPoint(i+1, 0);
            }
            if(accxrawdataPoints[i]==null){
                accxrawdataPoints[i] =  new DataPoint(i+1, 0);
            }
            if(accyrawdataPoints[i]==null){
                accyrawdataPoints[i] =  new DataPoint(i+1, 0);
            }
            if(acczrawdataPoints[i]==null){
                acczrawdataPoints[i] =  new DataPoint(i+1, 0);
            }
        }
        LineGraphSeries<DataPoint> series1 = new LineGraphSeries<>(gyrxrawdataPoints);
        series1.setColor(Color.rgb(255,0,0));
        series1.setThickness(2);
        LineGraphSeries<DataPoint> series2 = new LineGraphSeries<>(gyryrawdataPoints);
        series2.setColor(Color.rgb(0,255,0));
        series2.setThickness(2);
        LineGraphSeries<DataPoint> series3 = new LineGraphSeries<>(gyrzrawdataPoints);
        series3.setColor(Color.rgb(0,0,255));
        series3.setThickness(2);
        LineGraphSeries<DataPoint> series4 = new LineGraphSeries<>(accxrawdataPoints);
        series4.setColor(Color.rgb(255,0,0));
        series4.setThickness(2);
        LineGraphSeries<DataPoint> series5 = new LineGraphSeries<>(accyrawdataPoints);
        series5.setColor(Color.rgb(0,255,0));
        series5.setThickness(2);
        LineGraphSeries<DataPoint> series6 = new LineGraphSeries<>(acczrawdataPoints);
        series6.setColor(Color.rgb(0,0,255));
        series6.setThickness(2);

        graphView1.addSeries(series1);
        graphView1.addSeries(series2);
        graphView1.addSeries(series3);
        graphView2.addSeries(series4);
        graphView2.addSeries(series5);
        graphView2.addSeries(series6);


        graphView1.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    // show normal x values
                    return df.format(value*0.1);
                } else {
                    // show currency for y values
                    return super.formatLabel(value, isValueX);
                }
            }
        });
        graphView2.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    // show normal x values
                    return df.format(value*0.1);
                } else {
                    // show currency for y values
                    return super.formatLabel(value, isValueX);
                }
            }
        });

    }



}
